// 1. What is the term given to unorganized code that's very hard to work with?
        // spaghetti code

// 2. How are object literals written in JS?
        // {}

// 3. What do you call the concept of organizing information and functionality to belong to an object?
        //encapsulation 
// 4. If studentOne has a method named enroll(), how would you invoke it?
        // using dot notation
// 5. True or False: Objects can have objects as properties.
        // true
// 6. What is the syntax in creating key-value pairs?
        // let obj = {
        //     key: value
        // }
// 7. True or False: A method can have no parameters and still work.
        // true
// 8. True or False: Arrays can have objects as elements.
        // true
// 9. True or False: Arrays are objects.
        // true
// 10. True or False: Objects can have arrays as properties.
        // true